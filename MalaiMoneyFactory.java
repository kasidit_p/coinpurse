package coinpurse;

import java.util.Arrays;
import java.util.List;

/**
* This is subclass of MoneyFactory. This is Malaysia currency.
* @author Kasidit Phoncharoen
*
*/
public class MalaiMoneyFactory extends MoneyFactory{
	
	private final List<Double> coins = Arrays.asList(0.05, 0.10, 0.20, 0.50);
	private final List<Double> banknotes = Arrays.asList(1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0);
	
	/**
	 * Constructor for this class.
	 */
	public MalaiMoneyFactory(){
		super();
	}
	
	/**
	 *  A overide method of MoneyFactory.
	 */
	public Valuable createMoney(double value) {
		if(coins.contains(value)){
			return new Coin(value,AbstractValuable.Currency.MALAYSIA);
		} else if(banknotes.contains(value)){
			return new BankNote(value,AbstractValuable.Currency.MALAYSIA);
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Main method for testing this class.
	 */
	public static void main(String[] args) {
		MoneyFactory factory = MoneyFactory.getInstance();
		Valuable m = factory.createMoney( 5 );
		System.out.println(m.toString());
		Valuable m2 = factory.createMoney("1000.0");
		System.out.println(m2.toString());
		Valuable m3 = factory.createMoney(0.05);	
	}
}
