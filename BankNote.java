package coinpurse;

/**
 * A BankNote with a monetary value.
 * You can't change the value of a BankNote.
 * @author Kasidit Phoncharoen
 *
 */
public class BankNote extends AbstractValuable{
	private static int serialNumber = 999999;

	/**
	 * Constructor of BankNote.
	 * @param value of this BankNote
	 */
	public BankNote(double value, AbstractValuable.Currency currency){
		super(value, currency);
		getNextSerialNumber();
	}
	/**
	 * 
	 * @return next serial number.
	 */
	public static int getNextSerialNumber(){
		return serialNumber++;
	}
	/**
	 * @return string contain the value and serial number of this BankNote
	 */
	public String toString(){
		if(super.getCurrency().equals(AbstractValuable.Currency.MALAYSIA))
			return String.format("%.0f Ringgit Banknote",super.getValue());
		else 
			return String.format("%.0f Baht Banknote",super.getValue());
			
	}

}
