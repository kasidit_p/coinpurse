package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;
/**
 * RecursiveWithdraw class is Strategy Withdraw class contain method withdraw and withdrawFrom.
 * @author Kasidit Phoncharoen
 * @version PA2
 */
public class RecursiveWithdraw implements WithdrawStrategy {
	/**
	 * Find array of Valuable that watn to withdraw.
	 * @return array of Valuable that want to withdraw
	 * 		   null if can't with draw
	 */
	public Valuable[] withdraw(double amount, List<Valuable> list) {
		List<Valuable> templist = new ArrayList<Valuable>();
		for(int i = 0 ; i < list.size(); i++){
			templist = withdrawFrom(amount,list, i);
			if(templist != null)
				break;
		}
		if(templist != null){
			Valuable[] temp = new Valuable[templist.size()];
			templist.toArray(temp);
			return temp;
		}
		else
			return null;
	}

	/**
	 * Helper method of withdraw, help to find the list of valuable.
	 * @param remainingAmount is amount of money that want to withdraw
	 * @param list collection of Valuable that we put in.
	 * @param index of Valuable.
	 * @return list of Valuable that want to withdraw.
	 */
	public List<Valuable> withdrawFrom(double remainingAmount, List<Valuable> list, int index){

		if(index > list.size()-1 || remainingAmount-list.get(index).getValue() < 0 ){
			if(index < list.size())
				return withdrawFrom(remainingAmount,list,index+1);
			for(int i = 0 ; i < list.size(); i++){
				withdrawFrom(remainingAmount,list.subList(i, list.size()-1),0);
			}
			return null;
		}
		remainingAmount -= list.get(index).getValue();
		if(remainingAmount > 0){
			List<Valuable> temp = ( withdrawFrom(remainingAmount,list,index+1));
			if(temp != null){
				temp.add(list.get(index));
			}
			return  temp;
		}
		if(remainingAmount == 0){
			List<Valuable> temp = new ArrayList<Valuable>();
			temp.add(list.get(index));
			return temp;
		}
		return withdrawFrom(remainingAmount,list,index+1);
	}
}
