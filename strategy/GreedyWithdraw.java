package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;
import coinpurse.Valuable;
/**
 * GreedyWithdraw class is Strategy Withdraw class contain method withdraw.
 * @author Kasidit Phoncharoen
 * @version PA2
 */
public class GreedyWithdraw implements WithdrawStrategy{
	/**
	 * Find array of Valuable that watn to withdraw.
	 * @return array of Valuable that want to withdraw
	 * 		   null if can't with draw
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		List<Valuable> money = valuables;
		ArrayList<Valuable> templist = new ArrayList<Valuable>();
		int[] index = new int[money.size()];
		double balance = 0;
		for(int i = 0; i < money.size() ; i++){
			if(money.get(i).getValue() <= amount){
				if(balance+money.get(i).getValue() <= amount){
					balance+=money.get(i).getValue();
					templist.add(money.get(i));
					index[i]++;
				}
			}
			else{
				continue;
			}
		}
		if ( balance != amount ){	
			return null;
		}
		else{
			Valuable[] temp = new Valuable[templist.size()];
			templist.toArray(temp);

			double check = 0;
			for(int i = 0,j = 0  ; i < index.length ; i++){
				if(index[i]!=0 && check!=balance){
					check+= money.get(i-j).getValue();
					money.remove(i-j);
					j++;
				}
			}
			return temp;
		}
	}

}
