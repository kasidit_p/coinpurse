package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;
/**
 * 
 * @author Kasidit Phoncharoen
 *
 */
public interface WithdrawStrategy{
	/**
	 * Withdraw the requested amount of money.
	 * @param amount ,Amount of money that you want to withdraw.
	 * @param valuables ,Collection of money.
	 * @return array of Valuable(money).
	 */
	public Valuable[] withdraw(double amount,List<Valuable> valuables);
}
