package coinpurse;


import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Observable;

import coinpurse.strategy.WithdrawStrategy;

/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  @author Kasidit Phoncharoen
 */
public class Purse extends Observable{
	/** Collection of money in the purse. */
	List<Valuable> money;

	WithdrawStrategy strategy;

	private final ValueComparator comparator = new ValueComparator();

	/** Capacity is maximum NUMBER of coins the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;

	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of coins you can put in purse.
	 */
	public Purse( int capacity ) {
		this.capacity = capacity;
		money = new ArrayList<Valuable>();
	}

	/**
	 * Count and return the number of coins in the purse.
	 * This is the number of coins, not their value.
	 * @return the number of coins in the purse
	 */
	public int count() { 
		return money.size(); 
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {
		double balance = 0;
		for(int i = 0 ; i < money.size() ; i++){
			balance += money.get(i).getValue();
		}
		return balance; 
	}


	/**
	 * Return the capacity of the coin purse.
	 * @return the capacity
	 */
	public int getCapacity() { 
		return this.capacity; 
	}

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		if(money.size() == getCapacity())
			return true;
		return false;
	}

	/** 
	 * Insert a coin into the purse.
	 * The coin is only inserted if the purse has space for it
	 * and the coin has positive value.  No worthless coins!
	 * @param newMoney is a Coin object to insert into purse
	 * @return true if coin inserted, false if can't insert
	 */
	public boolean insert( Valuable newMoney ) {
		if(isFull() || newMoney.getValue()<=0)
			return false;
		else{
			money.add(newMoney);
			super.setChanged();
			super.notifyObservers(this);
			return true;
		}
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Coins withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		Collections.sort(money, comparator);
		Collections.reverse(money);
		Valuable[] temp = strategy.withdraw(amount, money);
		if(temp == null)
			return null;
		else{
			for(int i = 0; i < temp.length; i++){
				for(int j = 0; j < money.size(); j++){
					if(temp[i].getValue() == money.get(j).getValue()){
						money.remove(j);
						break;
					}
				}
			}
		}
		super.setChanged();
		super.notifyObservers(this);
		return temp;
	}

	/**
	 * Set strategy.
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy){

		this.strategy = strategy;
	}
	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 * @return String with value of this Purse
	 */
	public String toString() {
		return String.format("%d coins with value %.1f", this.count(), this.getBalance());
	}

}