package coinpurse;
/**
 * Abstract Superclass for Coin, Coupon and BankNote.
 * @author Kasidit Phoncharoen
 * @version lab4
 */
public abstract class AbstractValuable implements Valuable {
	private double value;
	private Currency currency;
	
	public static enum Currency{
		THAI("Thai"),
		MALAYSIA("Thai");
		public String name;
		Currency(String name){
			this.name = name;
		}
	}
	
	public AbstractValuable(double value){
		this.value = value;
	}
	public AbstractValuable(double value, Currency currency){
		this.value = value;
		this.currency = currency;
	}
	public Currency getCurrency(){
		return currency;
	}
	/**
	 * compare this Valuable and the argument Valuable.
	 * @param obj is the Valuable object
	 * @return 1 if this Valuable is less than other Valuable 
	 * 		  -1 if this Valuable is more than other Valuable 
	 *         0 if this Valuable is equal to other Valuable 
	 */
	public int compareTo(Valuable obj) {

		if(this.getValue() > obj.getValue())
			return 1;
		else if(this.getValue() < obj.getValue())
			return -1;
		else 
			return 0;
	}

	/**
	 * check if this object is equal to the argument or not.
	 * @param arg is the  Object
	 * @return true if the Object equal to this object
	 */
	public boolean equals(Object arg) { 	
		if(arg == null)
			return false;
		if(this.getClass() != arg.getClass())
			return false;
		Valuable obj = (Valuable) arg;
		if(this.getValue() ==  obj.getValue())
			return true;
		return false;
	}
	 /**
     *  @return the value of this Valuable. 
     * */

    public double getValue() {
    	return this.value;
    }
}
