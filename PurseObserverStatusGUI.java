package coinpurse;

import java.awt.Font;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
/**
 * GUI of status bar for Purse.
 * @author Kasidit Phoncharoen
 * @version Lab5
 */
public class PurseObserverStatusGUI extends JFrame implements Observer{
	private JProgressBar status;
	private JLabel text;
	public PurseObserverStatusGUI(){
		super("Purse Status");
	}
	/** Component for this GUI. This is boxlayout and have JProgressBar.*/
	public void initComponent(){
		JPanel panel = new JPanel();
		super.setSize(175,100);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		status = new JProgressBar();
		text = new JLabel("EMPTY");
		text.setFont(new Font("Serif", Font.PLAIN, 30));
		
		panel.add(text);
		panel.add(status);
		super.add(panel);
		
	}
	/** Run this GUI and set to be visible.*/
	public void run(){
		this.initComponent();
		super.setVisible(true);
	}
	/** Update recieve notification from the purse. */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			int size = purse.getCapacity();
			int contain = purse.count();
			this.status.setMaximum(size);
			this.status.setValue(contain);
			if(purse.isFull())
				this.text.setText("FULL");
			else if(contain == 0)
				this.text.setText("EMPTY");
			else
				this.text.setText(contain+"");
		}
		if(info != null) System.out.println(info);
	}
	
}
