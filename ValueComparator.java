package coinpurse;

import java.util.Comparator;

/**
 * compare 2 valuable with value.
 * @author Kasidit Phoncharoen
 */
public class ValueComparator implements Comparator<Valuable>{
	/**
	 *@param a Valuable Object
	 *@param b Valuable Object
	 * @return < 0 if a should be "before" b,
     *         > 0 if a should be "after" b,
     *         = 0 if a and b have same lexical order.
	 */
	public int compare(Valuable a, Valuable b){
		if(a.getValue() > b.getValue())
			return 1;
		if(a.getValue() < b.getValue())
			return -1;
		else
			return 0;
	}

}
