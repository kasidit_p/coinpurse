package coinpurse;

import java.util.Arrays;
import java.util.List;

/**
 * This is subclass of MoneyFactory. This is Thai currency.
 * @author Kasidit Phoncharoen
 *
 */
public class ThaiMoneyFactory extends MoneyFactory{

	private final List<Double> coins = Arrays.asList(1.0,2.0,5.0,10.0);
	private final List<Double> banknotes = Arrays.asList(20.0, 50.0, 100.0, 500.0, 1000.0);

	/**
	 * Constructor of this class.
	 */
	public ThaiMoneyFactory(){
		super();
	}

	/**
	 * A overide method of MoneyFactory.
	 */
	public Valuable createMoney(double value) {
		if(coins.contains(value)){
			return new Coin(value,AbstractValuable.Currency.THAI);
		} else if(banknotes.contains(value)){
			return new BankNote(value,AbstractValuable.Currency.THAI);
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Main class for testing this class.
	 */
	public static void main(String[] args) {
		MoneyFactory factory = MoneyFactory.getInstance();
		Valuable m = factory.createMoney( 5 );
		System.out.println(m.toString());
		Valuable m2 = factory.createMoney("1000.0");
		System.out.println(m2.toString());
		Valuable m3 = factory.createMoney(0.05);	
	}
}
