package coinpurse;

import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
/**
 * GUI of balance for Purse.
 * @author Kasidit Phoncharoen
 * @version 
 */
public class PurseObserverBalanceGUI extends JFrame implements Observer{
	private JLabel label;
	/** Counstructor for this GUI.*/
	public PurseObserverBalanceGUI(){
		super("Purse Balance");
	}
	/**Component for this GUI. This contain JLabel to label balance. */
	public void initComponent(){
		JPanel panel = new JPanel();
		super.setSize(175,50);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		label = new JLabel("0.0 Baht");
		label.setFont(new Font("Serif", Font.PLAIN, 30));
		
		panel.add(label);
		super.add(panel);
		
	}
	/** Run this GUI and set to be visible.*/
	public void run(){
		this.initComponent();
		super.setVisible(true);
	}
	/** Update recieve notification from the purse. */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			double balance = purse.getBalance();
			this.label.setText(balance+" Baht");
		}
		if(info != null) System.out.println(info);
	}
	
}
