package coinpurse;

 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Kasidit Phoncharoen
 */
public class Coin extends AbstractValuable{

    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value, AbstractValuable.Currency currency) {
    	super(value, currency);
    }
 
    /**
     * print this value.
     * @return this value
     */
    public String toString() {
    	if(super.getCurrency().equals(AbstractValuable.Currency.MALAYSIA))
    		return String.format("%.0f Sen coin", super.getValue());
    	else
    		return String.format("%.0f Bath coin", super.getValue());
    }
}
