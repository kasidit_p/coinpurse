package coinpurse;

import java.util.ResourceBundle;

/**
 * Encapsulate operation of creating money objects.
 * @author Kasidit Phonchaoren
 * @version Lab13
 */
public abstract class MoneyFactory {
	private static MoneyFactory instance;
	/**
	 * Constructor for this class; 
	 */
	public MoneyFactory() {
		instance = this;
	}

	/** Get the money factory instance. */
	public static MoneyFactory getInstance() {
		setFactory();
		return instance; 
	}
	
	/**
	 * get an instance of MoneyFactory. Use lazy instantiation. This means you create the singleton
	 * object the first time getInstance() is called.
	 * @param value of the money.
	 * @return Valuable 
	 */
	public abstract Valuable createMoney(double value);

	/**
	 * create a new money object in the local currency. Only
	 * accept valid values. If an invalid value of the
	 * parameter then throw IllegalArgumentException.
	 * @param value
	 * @return
	 */
	public Valuable createMoney(String value){
		double values = Double.parseDouble(value);
		return this.createMoney(values);
	}

	/**
	 * Change setting by a Properties or ResourceBundle from an InputStream or Reader.
	 */
	public static void setFactory(){
		ResourceBundle bundle = ResourceBundle.getBundle( "purse" );
		String value = bundle.getString( "currency" ); 
		try {
			instance = (MoneyFactory)Class.forName(value).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			instance = new ThaiMoneyFactory();
		}
	}

}