package coinpurse;

import java.util.Observer;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Kasidit Phoncharoen
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {

    	Purse purse = new Purse(-4);
    	RecursiveWithdraw strategy = new RecursiveWithdraw();
    	PurseObserverBalanceGUI balanceGUI = new PurseObserverBalanceGUI();
    	balanceGUI.run();
    	PurseObserverStatusGUI statusGUI = new PurseObserverStatusGUI();
    	statusGUI.run();
    	purse.addObserver(balanceGUI);
    	purse.addObserver(statusGUI);
    	purse.setWithdrawStrategy(strategy);
    	
    
    	ConsoleDialog ui = new ConsoleDialog(purse);
    	ui.run();
    	
    }
}
