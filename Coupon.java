package coinpurse;
import java.util.HashMap;
import java.util.Map;
/**
 * A coupon with a monetary value.
 * You can't change the value of a coupon.
 * @author Kasidit Phoncharoen
 *
 */
public class Coupon extends AbstractValuable{
	private String color;
	private static Map<String, Double> colors;
	static{
		colors = new HashMap<String, Double>();
		colors.put("red", 100.00);
		colors.put("blue", 50.00);
		colors.put("green", 20.00);
	}
	
	/** 
	 * Constructor of coupon.
	 * @param color is the color of this coupon 
	 * */
	public Coupon(String color, String currency){
		super((double)colors.get(color));
		this.color = color;
	}	
    /**
     * @return string of this color
     */
    public String toString(){
    	return String.format("%s coupon",this.color);
    }
	
}
