package coinpurse;
/**
 * Valuable Interface is a behavivor for many kind of money.
 * @author Kasidit Phoncharoen
 */
public interface Valuable extends Comparable{
	/**
	 * 
	 * @return value of that kind of money
	 */
	public double getValue( );
}
